# Dynamiska Webbapplikationer

### Uppsala Universitet, ST2019

* Applikationen nås [här](https://dynwebb.firebaseapp.com).

* Screencast återfinns [här](https://www.youtube.com/watch?v=XdKSuPfKTvw)

#### Validering

- [Index](https://validator.w3.org/nu/?doc=https%3A%2F%2Fdynwebb.firebaseapp.com%2Findex.html)
- [Kontakt](https://validator.w3.org/nu/?doc=https%3A%2F%2Fdynwebb.firebaseapp.com%2Ftemplates%2Fcontact.html)
- [Historia](https://validator.w3.org/nu/?doc=https%3A%2F%2Fdynwebb.firebaseapp.com%2Ftemplates%2Fhistory.html)
- [Gäster](https://validator.w3.org/nu/?doc=https%3A%2F%2Fdynwebb.firebaseapp.com%2Ftemplates%2Fguests.html)
- [Medlemmar](https://validator.w3.org/nu/?doc=https%3A%2F%2Fdynwebb.firebaseapp.com%2Ftemplates%2Fmembers.html)
- [Turister](https://validator.w3.org/nu/?doc=https%3A%2F%2Fdynwebb.firebaseapp.com%2Ftemplates%2Ftourists.html)
- [English](https://validator.w3.org/nu/?doc=https%3A%2F%2Fdynwebb.firebaseapp.com%2Ftemplates%2Fenglish.html)

© Arvid Edenheim 2019
