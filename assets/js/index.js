import { getWeatherForecastURI } from './util/constants.js'

let show
let menu
let weather

const toggleMenu = () => menu.classList.toggle('show')

const setWeatherTable = temperatureData => {
    const table = document.createElement('table')
    const headerRow = document.createElement('tr')

    if (temperatureData) {
        const tempRow = document.createElement('tr')
        headerRow.innerHTML += '<th>Tid</th>'
        tempRow.innerHTML += '<th>Temp</th>'
        temperatureData.forEach(entry => {
            headerRow.innerHTML += `<th>${entry.validTime.substr(11, 2)}</th>`
            const tempValues = entry.parameters.find(
                param => param.name === 't'
            ).values
            const temp =
                tempValues.reduce((a, b) => a + b, 0) / tempValues.length
            tempRow.innerHTML += `<td>${temp}</td>`
        })

        table.appendChild(headerRow)
        table.appendChild(tempRow)
    } else {
        headerRow.innerHTML = 'Väderdata är inte tillgängligt'
        table.appendChild(headerRow)
    }
    weather.appendChild(table)
}

const extractWeatherData = response => {
    let temperatureData
    if (response.status) {
        try {
            const { data } = response
            temperatureData = data.timeSeries.splice(0, 4)
        } catch (err) {}
    }
    setWeatherTable(temperatureData)
}

const fetchWeatherData = () =>
    fetch(getWeatherForecastURI())
        .then(res => res.json())
        .then(data => extractWeatherData({ status: true, data }))
        .catch(err => extractWeatherData({ status: false }))

const init = () => {
    menu = document.querySelector('#navbar')
    weather = document.querySelector('#weather')
    if(weather) {
       fetchWeatherData()
    }
}

document.toggleMenu = toggleMenu

document.addEventListener('DOMContentLoaded', init)
