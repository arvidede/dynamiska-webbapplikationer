const LON = 18.1489
const LAT = 57.3081

export const getWeatherForecastURI = (lon = LON, lat = LAT) =>
    `https://opendata-download-metfcst.smhi.se/api/category/pmp3g/version/2/geotype/point/lon/${lon}/lat/${lat}/data.json`
